import os
import re
import sys
import boto3
import random
import requests
import shorturl

sys.tracebacklimit=0

s3  = boto3.client('s3')
lambda_client = boto3.client('lambda')

bucket = os.environ['bucket']

api = 'https://' + os.environ['api_base'] + '/prod/upload-to-kek'

headers = {'Content-Type': 'text/plain'}

def check_name():
    try:
        s = shorturl.ShortURL()
        randnum = random.randint(0,2147483647)
        basename = s.encode(randnum)
        name = 'a/'+ basename 
        response =  s3.head_object(Bucket=bucket, Key=name)
        check_name() 
    except:
        return basename 

def lambda_handler(event, context):
    image = event['body']
    params = event['params']
    try:
        querystring = str(params).split('?')[1]
        old_album = requests.get('https://kek.gg/' + querystring)
        old_html = old_album.text
        old_html = old_html[:-13]
    except:
        old_html = '<html id="html" lang="en"><head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="description" content="free album hosting"><meta name="title" content="kek.gg - free image hosting"><meta name="keywords" content="free image hosting, free speech hosting,image host,sli.mg, slimg, slimgur, slim.gur, imgur"><title>KEK.gg/album</title><link href="/css/main.css" rel="stylesheet" type="text/css" /><div id="main" class="container">'
    try:
        r = requests.post(api, headers=headers, data=image)
        image_id = r.json()[2::]
        image_url = 'https://kek.gg/i/' + image_id
        html_image =  old_html + '<img alt="" class=gallery-image src="' + image_url +'"></div></html>'
    except:
        return '/'
    basename = check_name()
    name = 'a/'+ basename
    urlname = '?'+ name
    response = s3.put_object(Body=html_image, 
                   Bucket=bucket, 
                   Key=name, 
                   ContentType='text/html'
                   )
    return urlname
