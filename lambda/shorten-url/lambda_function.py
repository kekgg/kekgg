from bs4 import BeautifulSoup
import os
import re
import sys
import boto3
import random
import urllib2
import requests
import shorturl

sys.tracebacklimit=0

s3  = boto3.client('s3')

bucket = os.environ['bucket']

def check_name():
    try:
        s = shorturl.ShortURL()
        randnum = random.randint(0,7483647)
        basename = s.encode(randnum)
        name = 'u/'+ basename 
        response =  s3.head_object(Bucket=bucket, Key=name)
        check_name() 
    except:
        return basename 

def lambda_handler(event, context):
    image = ''
    description = ''
    html_header = ['<!doctype html>', 
                   '<html id="html" lang="en">', 
                    '<head><meta charset="utf-8" />', 
                     '<meta name="viewport" content="width=device-width, initial-scale=1.0">',
                     '<meta name="title" content="kek.gg/url - landing page">',
                     '<meta name="keywords" content="free image hosting, url shortener, free speech hosting,image host,sli.mg, slimg, slimgur, slim.gur, imgur">',
                     '<link href="/css/main.css" rel="stylesheet" type="text/css" />',
                    '</head>',
                    '<body>',
                     '<div id="main" class="container">',
                     '<header id=h>',
                     '<a href="/" style="text-decoration: none">',
                     '<!-- logo design by http://www.ramiropiedrabuena.com -->',
                     '<img class="logo" src="/i/5xvT47.png" alt="kek.gg logo" height=80px>',
                     '</a>',
                    '</header><br>']
    html_footer = [ '<div id="js-enabled"><center><h3 id="js-message"></h3></center></div>',
                    '<style>#js-enabled {display: none; color: green; padding: 4px; background: white; }</style>',
                    '<script>',
                    'document.write(\'<style>#backup-link {display:none;}</style>\');',
                    'document.write(\'<style>#js-enabled {display:block;}</style>\');',
                    'var url = document.getElementById("url").href;',
                    'function sleep(ms) { return new Promise(resolve => setTimeout(resolve, ms)); };',
                    'var updateHeader = async function (){',
                    'for (var i = 3; i >= 0;--i){',
                    'var innerString = \'redirecting to <a href="\' + url + \'">\' + url + \'</a> in \' + i + \' seconds\';',
                    'document.getElementById("js-message").innerHTML = innerString;',
                    'if (i === 0) { window.location = url };',
                    'await sleep(1000);',
                    '}};',
                    'var param = /[&?]([^&]+)/.exec(location.search);',
                    'param = param ? param[1].replace(/"/g, \'&quot;\') : \'\';',
                    'if (param === \'\'){updateHeader();}',
                    '</script>',
                    '</div>',
                    '<body>',
                   '</html>']
    url = event['body']
    try:
        is_http = re.match('http(s)?://', url)
        if is_http:
            url = url
        else:
            url = 'http://' + url
    except:
        return '/'
    try:
        r = requests.get(url)
        soup = BeautifulSoup(r.text)
        title = soup.title
        [s.extract() for s in soup('script')]
        [s.extract() for s in soup('style')]
        [s.extract() for s in soup.findAll('link', attrs={'rel': 'stylesheet'})]
        try:
            description = soup.find('meta', attrs={'property': 'og:description'})
            if not description:
                description = soup.find('meta', attrs={'property': 'twitter:description'})
            if not description:
                description = soup.find('meta', attrs={'name': 'description'})
            if not description:
                description = '<meta name="description" content="redirect to: ' + url + '">'
        except:
            print('error setting description')
            description = '<meta name="description" content="redirect to: ' + url + '">'

        try:
            image = soup.find('meta', attrs={'property': 'og:image'})
            if not image:
                image = soup.find('meta', attrs={'property': 'twitter:image'})
            if not image:
                image = soup.find(id='logo')
            if not image:
                image = soup.find('img')
        except:
            print('error setting image')
            pass
    except Exception as e:
        title = '<title>kek.gg url shortener</title>'
        print(url)
        print('error in main loop\n' + str(e))

    html_header.insert(3, str(image))
    html_header.insert(3, str(title))
    html_header.insert(3, str(description))
    html_main = '<h3 id="backup-link"><a id="url" href="'+ url + '">Click here to be redirected to ' + url + '</a></h3>'
    html = ''.join(html_header) + html_main + ''.join(html_footer)
    basename = check_name()
    name = 'u/'+ basename
    urlname = '?'+ name
    response = s3.put_object(Body=html, 
                   Bucket=bucket, 
                   Key=name, 
                   ContentType='text/html'
                   )
    return urlname
