import os
import re
import sys
import boto3
import base64
import random
import shorturl

sys.tracebacklimit=0

s3  = boto3.client('s3')
kms = boto3.client('kms')

bucket = os.environ['bucket']

def check_name(suffix):
    try:
        randnum = random.randint(0,2147483647)
        s = shorturl.ShortURL()
        basename = s.encode(randnum) + '.' + suffix
        name = 'i/'+ basename 
        response =  s3.head_object(Bucket=bucket, Key=name)
        check_name(suffix) 
    except:
        return basename 

def lambda_handler(event, context):
    image = event['body']
    try:
        suffix_re  = re.match('^data:image/(.+);base64,', image)
        suffix = str(suffix_re.group(1))
        if suffix == 'jpeg':
            suffix = 'jpg'
            header_suffix = 'jpeg'
        else:
            header_suffix = suffix
        imag  = re.sub('^data:image/.+;base64,', '', image)
    except:
        return '/'
    ima   = base64.b64decode(imag) 
    basename = check_name(suffix)
    name = 'i/'+ basename
    urlname = 'i?'+ basename
    response = s3.put_object(Body=ima, 
                   Bucket=bucket, 
                   Key=name, 
                   ContentType='image/' + header_suffix,
                   )
    return urlname
