import os
import re
import sys
import boto3
import base64
import random
import requests
import shorturl
from moviepy.editor import VideoFileClip

sys.tracebacklimit=0

s3  = boto3.client('s3')

bucket = os.environ['bucket']

twitter_prefix =  'https://video.twimg.com/tweet_video/'

def check_name(suffix):
    try:
        randnum = random.randint(0,2147483647)
        s = shorturl.ShortURL()
        basename = s.encode(randnum) + '.' + suffix
        name = 'i/'+ basename 
        response =  s3.head_object(Bucket=bucket, Key=name)
        check_name(suffix) 
    except:
        print('exception')
        return basename 

def lambda_handler(event, context):
    url = event['body']
    #try:
    get_response = requests.get(url)
    raw = get_response.content
    r = re.findall('https://pbs.twimg.com/tweet_video_thumb/(.*).jpg', raw)[0]
    video_url = 'https://video.twimg.com/tweet_video/' + r + '.mp4'
    get_response = requests.get(video_url)
    raw = get_response.content
    with open('/tmp/mp4', 'w') as f:
        f.write(raw)
        f.close()

    clip = VideoFileClip('/tmp/mp4')
    clip.write_gif('/tmp/out.gif')
    suffix = 'gif'
    basename = check_name(suffix)
    name = 'i/'+ basename
    urlname = 'i?'+ basename
    response = s3.upload_file('/tmp/out.gif', 
                   bucket, 
                   name, 
                   ExtraArgs={'ContentType': "image/gif"}
                   )
    return urlname
