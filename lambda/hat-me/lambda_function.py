import os
import cv2
import sys
import boto3
import random
import shorturl
import requests 

s3  = boto3.client('s3')

bucket = os.environ['bucket']

cascPath = 'haarcascade_frontalface_default.xml'

sys.tracebacklimit=0

def check_name(suffix):
    try:
        randnum = random.randint(0,2147483647)
        s = shorturl.ShortURL()
        basename = s.encode(randnum) + '.' + suffix
        name = 'i/'+ basename 
        response =  s3.head_object(Bucket=bucket, Key=name)
        check_name(suffix) 
    except:
        return basename 


def lambda_handler(event, context):
    try:
        print(event)
        image_path = '/tmp/trump.jpg'
        url = event['body']
        r = requests.get(url)
        with open(image_path, 'w') as f:
            f.write(r.content)
            f.close()

        hat_file = 'maga.png'
        output_image = '/tmp/trump_with_hat.jpg'
        
        suffix = output_image.split('.')[-1]
        print(suffix)
        basename = check_name(suffix)
        name = 'i/'+ basename
        urlname = 'i?'+ basename

        # Create the haar cascade
        faceCascade = cv2.CascadeClassifier(cascPath)
        
        # Load our overlay image: hat.png
        imgHat = cv2.imread(hat_file, cv2.IMREAD_UNCHANGED)
        # Create the mask for the hat 
        orig_mask = imgHat[:,:,3]
        # Create the inverted mask for the hat
        orig_mask_inv = cv2.bitwise_not(orig_mask)
        
        # Convert hat image to BGR
        # and save the original image size (used later when re-sizing the image)
        imgHat = imgHat[:,:,0:3]
        orighatHeight, origHatWidth = imgHat.shape[:2]
        
        # Read the image
        image = cv2.imread(image_path)
        #print('about to read image')
        #if image.empty():
        #    print('returning image empty')
        #    return 'https://kek.gg/h.html?image_empty'

        print('made it past first if')
        #if image.channels() > 1:
        print('gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)')
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        #else:
        #    print('gray=image')
        #    gray = image

        print('completed if statement')
        
        # Detect faces in the image
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            #flags = cv2.cv.CV_HAAR_SCALE_IMAGE
            flags = 0
        )
        
        print("Found {0} faces!".format(len(faces)))
        
        if len(faces) == 0:
                return "https://kek.gg/h.html?no_face_found"

        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            # The hat should be 1.2 times the width of the face
            hatWidth =  w + int(w * 0.5)
            hatHeight = hatWidth * origHatWidth / origHatWidth
           
            hat = cv2.resize(imgHat, (hatWidth,hatHeight), interpolation = cv2.INTER_AREA)
           
            # Center the hat the top of face
            #higher x value moves left
            x1 = x - (hatWidth * 0.2)
            x2 = x + w + (hatWidth * 0.2)
            y1 = y - hatHeight + int(hatHeight * 0.35)
            y2 = y + int(hatHeight * 0.35)
            cropImg = 0
           
            # Check for clipping
            if x1 < 0:
              x1 = 0
            if x2 > image.shape[0]:
              x2 = w
            if y1 < 0:
              y1 = 0
              cropImg = hat.shape[1] - y2
              hat = hat[cropImg:]
            if y2 > image.shape[1]:
              y2 = h
           
            # Find region of interest
            roi_gray = gray
            roi_color = image
           
            # Re-size the original image and the masks to the hat sizes
            # calcualted above
            mask = cv2.resize(orig_mask, (hatWidth,hatHeight), interpolation = cv2.INTER_AREA)
            mask = mask[cropImg:]
            mask_inv = cv2.resize(orig_mask_inv, (hatWidth,hatHeight), interpolation = cv2.INTER_AREA)
            mask_inv = mask_inv[cropImg:]
            # Take ROI for hat from background equal to size of hat image
            roi = roi_color[int(y1):int(y2), int(x1):int(x1)+hatWidth]
            # roi_bg contains the original image only where the hat is not
            # in the region that is the size of the hat.
            roi_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
            # roi_fg contains the image of the hat only where the hat is
            roi_fg = cv2.bitwise_and(hat,hat,mask = mask)
           
            # Join the roi_bg and roi_fg
            dst = cv2.add(roi_bg,roi_fg)
            # Place the joined image, saved to dst back over the original image
            roi_color[int(y1):int(y2), int(x1):int(x1)+hatWidth] = dst
        
        cv2.imwrite(output_image, roi_color)
        response = s3.upload_file(output_image, bucket, name, ExtraArgs={'ContentType':'image/' + suffix})
        return "https://kek.gg/" + urlname
    except:
        return "https://kek.gg/h.html?could_not_find_a_face"
