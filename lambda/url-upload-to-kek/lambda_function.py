import os
import re
import sys
import imghdr
import base64
import requests

sys.tracebacklimit=0

api = 'https://' + os.environ['api_base'] + '/prod/upload-to-kek'
headers = {'Content-Type': 'text/plain'}


def lambda_handler(event, context):
    url = event['body']
    try:
        get_response = requests.get(url)
        raw_image = get_response.content
        image_format = imghdr.what('', raw_image)
    except:
        return '/?error=could_not_download_image'
    image = 'data:image/' + image_format + ';base64,' + base64.b64encode(raw_image)
    try:
        r = requests.post(api, headers=headers, data=image)
        image_id = r.json()
    except:
        return '/'
    return image_id
