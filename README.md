# kekgg
to-do:
------

general
-------
- gallery
- ~~Ctrl + V uploads image from clipboard~~

draw.html
---------
- ~~manually edit size of canvas~~
- ~~add fonts: Lusitana, Gloria Hallelujah, Pirata One Simple, Cabin~~
- ~~controls for moving elements front to back~~
- ~~color for text (and outline)~~ 
- ~~more intuitive object layering~~
- ~~make image layering static, don't change when dragging images~~
- ~~add text to canvas without adding an image~~
- ~~move objects using arrow keys~~
- change font & color of existing text box 
- better font customization - maybe 10 base fonts, then import your own
- upload pic by URL 
- drop shadow effect
- templates for common memes like the upgrade meme
- undo/redo
- keyboard shortcuts
- delete individual items
- canvas selectable list of sizes
- snap to edges
- snap to grid
- show grid
