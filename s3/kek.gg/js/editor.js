(function(){
  var mainScriptEl = document.getElementById('main');
  if (!mainScriptEl) return;
  var preEl = document.createElement('pre');
  var codeEl = document.createElement('code');
  codeEl.innerHTML = mainScriptEl.innerHTML;
  codeEl.className = 'language-javascript';
  preEl.appendChild(codeEl);
})();

(function() {
  var $ = function(id){return document.getElementById(id)};

  var canvas = this.__canvas = new fabric.Canvas('c', {
    isDrawingMode: false
  });
    var imgElement = document.getElementById('my-image');
    var imgInstance = new fabric.Image(imgElement, {
    left: 10,
    top: 50,
    //angle: 30,
    //opacity: 0.85
    });
    canvas.add(imgInstance);
    canvas.add(new fabric.IText('Tap and Type', { 
      fontFamily: 'arial black',
      left: 20, 
      top: 0 ,
      centeredRotation: true,
      centeredScaling: true,
    }))
    canvas.add(new fabric.IText('Tap and Type', { 
      fontFamily: 'arial black',
      left: 20, 
      top: (imgInstance.height + 50),
      centeredRotation: true,
      centeredScaling: true,
    }))

  canvas.setHeight((imgInstance.height) + 100);
  canvas.setWidth((imgInstance.width) + 50);

  fabric.Object.prototype.transparentCorners = false;

  var drawingModeEl = $('drawing-mode'),
      addTextBoxEl = $('add-text-box'),
      drawingOptionsEl = $('drawing-mode-options'),
      drawingColorEl = $('drawing-color'),
      drawingShadowColorEl = $('drawing-shadow-color'),
      drawingLineWidthEl = $('drawing-line-width'),
      drawingShadowWidth = $('drawing-shadow-width'),
      drawingShadowOffset = $('drawing-shadow-offset'),
      clearEl = $('clear-canvas');

  clearEl.onclick = function() { canvas.clear() };

//  drawingModeEl.onclick = function() {
//    canvas.isDrawingMode = !canvas.isDrawingMode;
//    if (canvas.isDrawingMode) {
//      drawingModeEl.innerHTML = 'Cancel drawing mode';
//      drawingOptionsEl.style.display = '';
//    }
//    else {
//      drawingModeEl.innerHTML = 'Enter drawing mode';
//      drawingOptionsEl.style.display = 'none';
//    }
//  };


  addTextBoxEl.onclick = function() {
    var randomnumber = Math.floor(Math.random()*(51 + imgInstance.height));
    canvas.add(new fabric.IText('Click to Edit', { 
      fontFamily: 'arial black',
      left: 20, 
      top: randomnumber,
      centeredRotation: true,
      centeredScaling: true,
    }))
  }; 

  if (fabric.PatternBrush) {
    var vLinePatternBrush = new fabric.PatternBrush(canvas);
    vLinePatternBrush.getPatternSrc = function() {

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = 10;
      var ctx = patternCanvas.getContext('2d');

      ctx.strokeStyle = this.color;
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(0, 5);
      ctx.lineTo(10, 5);
      ctx.closePath();
      ctx.stroke();

      return patternCanvas;
    };

    var hLinePatternBrush = new fabric.PatternBrush(canvas);
    hLinePatternBrush.getPatternSrc = function() {

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = 10;
      var ctx = patternCanvas.getContext('2d');

      ctx.strokeStyle = this.color;
      ctx.lineWidth = 5;
      ctx.beginPath();
      ctx.moveTo(5, 0);
      ctx.lineTo(5, 10);
      ctx.closePath();
      ctx.stroke();

      return patternCanvas;
    };

    var squarePatternBrush = new fabric.PatternBrush(canvas);
    squarePatternBrush.getPatternSrc = function() {

      var squareWidth = 10, squareDistance = 2;

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
      var ctx = patternCanvas.getContext('2d');

      ctx.fillStyle = this.color;
      ctx.fillRect(0, 0, squareWidth, squareWidth);

      return patternCanvas;
    };

    var diamondPatternBrush = new fabric.PatternBrush(canvas);
    diamondPatternBrush.getPatternSrc = function() {

      var squareWidth = 10, squareDistance = 5;
      var patternCanvas = fabric.document.createElement('canvas');
      var rect = new fabric.Rect({
        width: squareWidth,
        height: squareWidth,
        angle: 45,
        fill: this.color
      });

      var canvasWidth = rect.getBoundingRectWidth();

      patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
      rect.set({ left: canvasWidth / 2, top: canvasWidth / 2 });

      var ctx = patternCanvas.getContext('2d');
      rect.render(ctx);

      return patternCanvas;
    };

    var img = new Image();
    img.src = '/logo.png';

    var texturePatternBrush = new fabric.PatternBrush(canvas);
    texturePatternBrush.source = img;
    //canvas.add(img);
  }

  $('drawing-mode-selector').onchange = function() {

    if (this.value === 'hline') {
      canvas.freeDrawingBrush = vLinePatternBrush;
    }
    else if (this.value === 'vline') {
      canvas.freeDrawingBrush = hLinePatternBrush;
    }
    else if (this.value === 'square') {
      canvas.freeDrawingBrush = squarePatternBrush;
    }
    else if (this.value === 'diamond') {
      canvas.freeDrawingBrush = diamondPatternBrush;
    }
    else if (this.value === 'texture') {
      canvas.freeDrawingBrush = texturePatternBrush;
    }
    else {
      canvas.freeDrawingBrush = new fabric[this.value + 'Brush'](canvas);
    }

    if (canvas.freeDrawingBrush) {
      canvas.freeDrawingBrush.color = drawingColorEl.value;
      canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
      canvas.freeDrawingBrush.shadowBlur = parseInt(drawingShadowWidth.value, 10) || 0;
    }
  };

  drawingColorEl.onchange = function() {
    canvas.freeDrawingBrush.color = this.value;
  };
  drawingShadowColorEl.onchange = function() {
    canvas.freeDrawingBrush.shadowColor = this.value;
  };
  drawingLineWidthEl.onchange = function() {
    canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
    this.previousSibling.innerHTML = this.value;
  };
  drawingShadowWidth.onchange = function() {
    canvas.freeDrawingBrush.shadowBlur = parseInt(this.value, 10) || 0;
    this.previousSibling.innerHTML = this.value;
  };
  drawingShadowOffset.onchange = function() {
    canvas.freeDrawingBrush.shadowOffsetX =
    canvas.freeDrawingBrush.shadowOffsetY = parseInt(this.value, 10) || 0;
    this.previousSibling.innerHTML = this.value;
  };

  if (canvas.freeDrawingBrush) {
    canvas.freeDrawingBrush.color = drawingColorEl.value;
    canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
    canvas.freeDrawingBrush.shadowBlur = 0;
  }
})();

//(function() {
//  fabric.util.addListener(fabric.window, 'load', function() {
//    var canvas = this.__canvas || this.canvas,
//        canvases = this.__canvases || this.canvases;
//
//    canvas && canvas.calcOffset && canvas.calcOffset();
//
//    if (canvases && canvases.length) {
//      for (var i = 0, len = canvases.length; i < len; i++) {
//        canvases[i].calcOffset();
//      }
//    }
//  });
//})();

